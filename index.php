<?php
require_once "config/base.php";


$url = $_GET['url'];
$urla = explode('/', $url);

if (isset($urla[0]) && !empty($urla[0])) {
     $cn = ucfirst($urla[0]) . 'Controller';
     $cf = 'controller/' . $cn . '.php';
    if (file_exists($cf)) {
        require_once $cf;
        $object= new $cn;
        if (isset($urla[1]) && !empty($urla[1])) {
            $method = $urla[1];
            if(method_exists($object,$method)){
                $object->$method();

            }else{
                die ("$method doesnt exist in $cn");
            }

        }else {
            die(" method doesn't exist");
        }
    } else {
        die("$cn file doesn't exist");
    }

}

